import numpy as np

# matplotlib for displaying the output
import matplotlib.pyplot as plt
import matplotlib.style as ms

ms.use('seaborn-muted')

# Librosa for audio
import librosa
# And the display module for visualization
import librosa.display


def calc_logamplitude_spectrogram(audio_path):
    y, sr = librosa.load(audio_path, duration=10.0)
    # Let's make and display a mel-scaled power (energy-squared) spectrogram
    S = librosa.feature.melspectrogram(y, sr=sr, n_mels=256)

    # Convert to log scale (dB). We'll use the peak power as reference.
    log_S = librosa.logamplitude(S, ref_power=np.max)
    return sr, log_S


if __name__ == '__main__':
    audio_path = './data/train/1272/135031/1272-135031-0000.flac'
    sr, log_S = calc_logamplitude_spectrogram(audio_path)

    # Make a new figure
    plt.figure(figsize=(12, 8))

    # Display the spectrogram on a mel scale
    # sample rate and hop length parameters are used to render the time axis
    librosa.display.specshow(log_S, sr=sr, x_axis='time', y_axis='mel')

    # Put a descriptive title on the plot
    plt.title('mel power spectrogram')

    # draw a color bar
    plt.colorbar(format='%+02.0f dB')

    # Make the figure layout compact
    plt.tight_layout()
    plt.show()
