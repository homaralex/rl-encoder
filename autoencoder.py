import os
from random import shuffle

import numpy as np
from keras.utils.visualize_util import plot
from encoder import models

MAX_SOUND_LEN = 200000
SAMPLE_RATE = 16 * 1000
LEN_FREQS = 128
LEN_TIME = 598

TRAIN_PATH = './data/train'
TRAIN_PATH_SPECTR = './data/train_spectr/data.npz'
TEST_PATH = './data/test'
TEST_PATH_SPECTR = './data/test_spectr/data.npz'


class FileLoader:
    @staticmethod
    def load_spectrogram(file_path):
        import librosa
        y, sr = librosa.load(file_path, sr=SAMPLE_RATE)
        # pad the data with zeros if its not big enough
        data_diff = MAX_SOUND_LEN - len(y)
        if data_diff > 0:
            y = np.append(y, np.zeros(data_diff))
        else:
            y = y[:MAX_SOUND_LEN]
        S = librosa.feature.melspectrogram(y, sr=sr)
        # Convert to log scale (dB). We'll use the peak power as reference.
        log_S = librosa.logamplitude(S, ref_power=np.max)
        return sr, log_S

    @staticmethod
    def find_samples(data_path, extension='.flac'):
        flac_files = []
        for root, subdirs, files in os.walk(data_path):
            flac_files.extend(
                [os.path.join(root, filename) for filename in files if filename.endswith(extension)])
        return flac_files

    @staticmethod
    def gen_data(data_path, is_conv):
        samples = FileLoader.find_samples(data_path)
        shuffle(samples)
        for sample_path in samples:
            sr, logS = FileLoader.load_spectrogram(sample_path)
            # add gaussian noise to the input
            noised = np.random.normal(logS, 1)
            shape0 = logS.shape[0]
            shape1 = logS.shape[1]
            if is_conv:
                logS = logS.reshape(1, shape0, shape1, 1)
                noised = noised.reshape(1, shape0, shape1, 1)
            else:
                logS = logS.reshape(1, shape0 * shape1)
                noised = noised.reshape(1, shape0 * shape1)
            yield (noised, logS,)

    @staticmethod
    def gen_data_preprocessed(data_path, is_conv):
        npz_file = np.load(data_path)
        for file in npz_file.files:
            logS = npz_file[file]
            noised = np.random.normal(logS, 8)
            shape0 = logS.shape[0]
            shape1 = logS.shape[1]
            if is_conv:
                logS = logS.reshape(1, shape0, shape1, 1)
                noised = noised.reshape(1, shape0, shape1, 1)
            else:
                logS = logS.reshape(1, shape0 * shape1)
                noised = noised.reshape(1, shape0 * shape1)
            yield (noised, logS,)


def make_model():
    model, encoder = models.conv2(LEN_FREQS, 391)
    plot(model, to_file='model.png', show_shapes=True)
    return model, encoder


def run_model(model, encoder):
    nb_samples = 16
    nb_epoch = int(nb_samples * 32 / nb_samples)
    model.fit_generator(generator=FileLoader.gen_data_preprocessed(TRAIN_PATH_SPECTR, model.is_conv),
                        samples_per_epoch=nb_samples,
                        nb_epoch=nb_epoch,
                        validation_data=FileLoader.gen_data_preprocessed(TEST_PATH_SPECTR, model.is_conv),
                        nb_val_samples=nb_samples)

    print('Model weights:\n{}'.format(model.get_weights()))
    from encoder.utils import plot_mel_spectrogram
    samples = FileLoader.find_samples(TEST_PATH)
    shuffle(samples)
    for i in range(1):
        sample_path = samples[i]
        print('\n', sample_path)
        sr, log_S = FileLoader.load_spectrogram(sample_path)
        plot_mel_spectrogram(sr, log_S)
        if model.is_conv:
            x = log_S.reshape(1, log_S.shape[0], log_S.shape[1], 1)
        else:
            x = log_S.reshape(1, log_S.shape[0] * log_S.shape[1])
        prediction = model.predict(x)
        print(prediction)
        plot_mel_spectrogram(sr, prediction.reshape(log_S.shape[0], log_S.shape[1]))
        encoding = encoder.predict(x)[0]
        for filter_idx in range(5):
            plot_mel_spectrogram(sr, encoding[:, :, filter_idx * 2:filter_idx * 2 + 1].reshape(log_S.shape[0],
                                                                                               log_S.shape[1]))


if __name__ == '__main__':
    run_model(*make_model())
