import matplotlib.pyplot as plt


def plot_fft(fft_data):
    plt.plot(abs(fft_data[:(len(fft_data) / 2 - 1)]), 'r')
    plt.show()


def plot_spectrogram(t, f, Sxx):
    plt.pcolormesh(t, f, Sxx)
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.show()


def plot_mel_spectrogram(sr, log_S, block=True):
    # Make a new figure
    plt.figure(figsize=(12, 8))

    # Display the spectrogram on a mel scale
    # sample rate and hop length parameters are used to render the time axis
    import librosa
    librosa.display.specshow(log_S, sr=sr, x_axis='time', y_axis='mel')

    # Put a descriptive title on the plot
    plt.title('mel power spectrogram')

    # draw a color bar
    plt.colorbar(format='%+02.0f dB')

    # Make the figure layout compact
    plt.tight_layout()
    plt.show(block=block)
