import os

import sys
from etaprogress.progress import ProgressBar
from encoder.autoencoder import FileLoader, TRAIN_PATH, TEST_PATH

import numpy as np

if __name__ == '__main__':
    for path in [TRAIN_PATH, TEST_PATH]:
        spectr_dir = '{}_spectr'.format(path)
        if not os.path.isdir(spectr_dir):
            os.makedirs(spectr_dir)
        spectrograms_path = os.path.join(spectr_dir, 'data.npz')
        samples = FileLoader.find_samples(TRAIN_PATH)
        data = {}
        progress_bar = ProgressBar(len(samples))
        for sample_path in samples:
            sr, logS = FileLoader.load_spectrogram(sample_path)
            data[sample_path] = logS
            progress_bar.numerator += 1
            sys.stdout.write('\r{}'.format(progress_bar))
            sys.stdout.flush()

        print('\nSaving to {}'.format(spectrograms_path))
        np.savez(spectrograms_path, **data)
