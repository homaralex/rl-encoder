from keras.engine import Input
from keras.engine import Model
from keras.layers import Convolution2D, MaxPooling2D, UpSampling2D, Cropping2D, ZeroPadding2D, Dense, AveragePooling2D, \
    LeakyReLU, Dropout, BatchNormalization, GaussianNoise
from keras.regularizers import activity_l2, l2


def conv1(len_f, len_t):
    input_shape = Input(shape=(len_f, len_t, 1))
    encoded = Convolution2D(16, 5, 5, activation='linear', border_mode='valid', init='uniform')(input_shape)

    x = encoded
    x = Convolution2D(16, 5, 5, activation='linear', border_mode='same', init='uniform')(x)
    x = ZeroPadding2D(padding=(0, 4, 0, 4))(x)
    decoded = Convolution2D(1, 3, 3, activation='linear', border_mode='same', init='uniform')(x)

    encoder = Model(input_shape, encoded)
    model = Model(input_shape, decoded)
    model.compile(optimizer='adadelta', loss='mse')
    model.__setattr__('is_conv', True)
    return model, encoder


def conv2(len_f, len_t):
    input_shape = Input(shape=(len_f, len_t, 1))
    encoded = Convolution2D(32, 3, 3, activation=LeakyReLU(), border_mode='same', init='normal',
                            W_regularizer=l2(0.01), activity_regularizer=activity_l2(0.01))(input_shape)

    x = encoded
    x = Convolution2D(32, 3, 3, activation=LeakyReLU(), border_mode='same', init='normal')(x)
    decoded = Convolution2D(1, 2, 2, activation='linear', border_mode='same', init='normal')(x)

    encoder = Model(input_shape, encoded)
    decoder = Model(input_shape, decoded)
    decoder.compile(loss='mse', optimizer='adadelta')
    decoder.__setattr__('is_conv', True)
    return decoder, encoder


def dense1(len_f, len_t):
    input_shape = Input(shape=(len_f * len_t,))
    init = 'zero'
    encoded = Dense(128, activation='relu', init=init)(input_shape)
    encoded = Dense(64, activation='relu', init=init)(encoded)
    encoded = Dense(32, activation='relu', init=init)(encoded)

    decoded = Dense(64, activation='relu')(encoded)
    decoded = Dense(128, activation='relu')(decoded)
    decoded = Dense(len_f * len_t, activation='sigmoid')(decoded)

    encoder = Model(input_shape, decoded)
    model = Model(input_shape, decoded)
    model.compile(loss='binary_crossentropy', optimizer='Adagrad')
    model.__setattr__('is_conv', False)
    return model, encoder
